import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { increment, decrement, signIn } from './actions'

function App() {
  const counter = useSelector(state => state.counter)
  const isLogged = useSelector(state => state.isLogged)
  const dispatch = useDispatch()
  return (
    <div className="App">
      <h1>Counter {counter}</h1>
      <button onClick={() => dispatch(increment())}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
      {isLogged ? <h3>Secret</h3> : <h3>Not available</h3>}
      <button onClick={() => dispatch(signIn())}>Login here to see the secret content</button>

    </div>
  );
}

export default App;
